
const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id, 
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});

	// .sign(<data>, <secret_key>, {})	
}

module.exports.verify = (req, res, next) => {

	//get the token in the headers authorization
	let token = req.headers.authorization.slice(7);
	console.log(token);

	if(typeof token !== "undefined"){

		//jwt.verify(token, secret, callback(error, data))
		return jwt.verify(token, secret, (error, result) => 
			error ? res.send({auth: "failed"}) : next());
	}
}

module.exports.decode = (token) => {

	token = token.slice(7);

	if(typeof token !== "undefined"){

		return jwt.verify(token, secret, (error, result) => 
			error ? null : jwt.decode(token, {complete: true}).payload);
	}

/*	if(typeof token != "undefined"){
		console.log(jwt.decode(token, {complete: true}))
	}*/
}

