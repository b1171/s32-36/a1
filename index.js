
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const PORT = process.env.PORT || 4000;

//after exporting router module, import it to index.js file
const courseRoute = require('./routes/courseRoutes');
const userRoute = require('./routes/userRoutes');

//mongodb connection
mongoose.connect('mongodb+srv://admin:admin-1996%2F%2A@batch139.ppmgo.mongodb.net/course-booking?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true});

//mongodb notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}))

//routes
app.use("/api/courses", courseRoute);
app.use("/api/users", userRoute);

//middleware that passes all requests to userRoutes module


app.listen(PORT, () => console.log(`Server running at port ${PORT}`));

