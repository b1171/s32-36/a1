
const express = require('express');
const router = express.Router();
const userController = require('./../controllers/userControllers');
const auth = require('./../auth');

router.post('/register', (req, res) => {

	userController.register(req.body).then(
		result => res.send(result));
})

router.get('/', (req, res) => {

	userController.getAllUsers().then(
		result => res.send(result));
})

router.get('/email-exists', (req, res) => {

	userController.checkEmail(req.body).then(
		result => res.send(result));
})

router.post('/login', (req, res) => {

	userController.login(req.body).then(
		result => res.send(result));
})

router.get('/details', auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization);

	userController.getProfile(userData).then(
		result => res.send(result));
})

module.exports = router;