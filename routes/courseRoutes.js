
const express = require('express');
const router = express.Router();
const auth = require('./../auth');
const courseController = require("./../controllers/courseControllers");

//create course
router.post("/create-course", auth.verify, (req, res) => {
	courseController.createCourse(req.body).then(
		result => res.send(result));
})

//get all courses
router.get("/", (req, res) => {
	courseController.getAllCourses().then(
		result => res.send(result));
})

//retrieving only active courses
router.get("/active-courses", (req, res) => {
	courseController.getActiveCourses().then(
		result => res.send(result));
})

//get a specific course using findOne()
router.get("/get-course", (req, res) => {
	courseController.getSpecificCourse(req.body).then(
		result => res.send(result));
})

//get a specific course using findById()
router.get("/:id/get-course", (req, res) => {
	courseController.getSpecificCourseById(req.params.id).then(
		result => res.send(result));
})

//update isActive status of the course using findOneAndUpdate()
router.put("/update-course-status", (req, res) => {
	courseController.updateCourseStatus(req.body).then(
		result => res.send(result));
})

//update isActive status of the course using findByIdAndUpdate()
router.put("/:id/update-course-status/", (req, res) => {
	courseController.updateCourseStatusById(req.params.id, req.body).then(
		result => res.send(result));
})

//delete course using findOneAndDelete()
router.delete("/delete-course", (req, res) => {
	courseController.deleteCourse(req.body).then(
		result => res.send(result));
})

//delete course using findByIdAndDelete()
router.delete("/:id/delete-course/", (req, res) => {
	courseController.deleteCourseById(req.params.id).then(
		result => res.send(result));
})

module.exports = router;