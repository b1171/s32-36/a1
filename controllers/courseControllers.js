
const Course = require('./../models/Course');

module.exports.createCourse = (reqBody) => {

	let newCourse = new Course(reqBody);

	return newCourse.save().then((result, error) => 
		error ? false : `${reqBody.courseName} has been created` );
}

module.exports.getAllCourses = () => {

	return Course.find().then((result, error) =>
		error ? false: result );
}

module.exports.getActiveCourses = () => {

	return Course.find({isActive: true}).then((result, error) =>
		error ? false : result );
}

module.exports.getSpecificCourse = (reqBody) => {

	return Course.findOne(reqBody).then((result, error) =>
		error ? false : (result === null ? `Not found` : result));
}

module.exports.getSpecificCourseById = (id) => {

	return Course.findById(id).then((result, error) =>
		error ? false : (result === null ? `Not found` : result));
}

module.exports.updateCourseStatus = (reqBody) => {

	return Course.findOneAndUpdate({courseName: reqBody.courseName},
		{isActive: reqBody.isActive}).then((result, error) =>
		error? false: Course.findOne({courseName: reqBody.courseName})).then((result, error) =>
		error ? false : result);
}

module.exports.updateCourseStatusById = (id, reqBody) => {

	return Course.findByIdAndUpdate(id, {isActive: reqBody.isActive}).then((result, error) =>
		error ? false: Course.findById(id)).then((result, error) =>
		error ? false : result);
}

module.exports.deleteCourse = (reqBody) => {
	
	return Course.findOneAndDelete(reqBody).then((result, error) =>
		error ? false : `${result.courseName} has been deleted`) ;
}

module.exports.deleteCourseById = (id) => {

	return Course.findByIdAndDelete(id).then((result, error) =>
		error ? false : `${id} has been deleted`);
}
