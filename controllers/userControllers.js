
const User = require('./../models/User');
const bcrypt = require('bcrypt');
const auth = require('./../auth');

module.exports.checkEmail = (reqBody) => {

	const {email} = reqBody;

	return User.findOne({email: email}).then( (result, error) => {
		// console.log(email)
		if(result != null){
			return `Email already exists`;
		} else {
			console.log(result)	//null bec email does not exist
			return result == null ? true : error;
		}
	})

}

module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})

	//save()
	return newUser.save().then( (result, error) => 
		result ? true : error);

}

module.exports.getAllUsers = () => {

	return User.find().then((result, error) =>
		error ? error : result);
}

module.exports.login = (reqBody) => {

	const {email, password} = reqBody;

	return User.findOne({email: email}).then((result, error) => {
		if(result == null){
			return false;
		} else{

			let isPassword = bcrypt.compareSync(
				password, result.password);

			if(isPassword){
				return {access: auth.createAccessToken(result)};
			} else{
				return false;
			}
		}
	})
}

module.exports.getProfile = (user) => {

	const {id} = user;

	return User.findOne({id: id}).then((result, error) => {
		if(error){
			return error;
		} else{
			result.password = "";
			return result;
		}
	})
}



